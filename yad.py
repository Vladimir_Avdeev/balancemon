# -*- coding: utf-8 -*-
import json
from queue import periodic_queue
from http_request import Http_request
from datetime import datetime
import mongodb_conn
import influxdb_conn


def check_balance():
    for wallet in check_balance.wallets.find():
        url = "https://money.yandex.ru/api/account-info"
        data = Http_request.request(url, None, ['Content-Type: application/x-www-form-urlencoded',
                                                "Authorization: Bearer {:s}".format(wallet["token"])])
        try:
            account_info_resp = json.loads(data)
            available = float(account_info_resp["balance_details"]["available"])
            total = float(account_info_resp["balance_details"]["total"])
            account = account_info_resp["account"]

        except NameError:
            print 'Responce parsing error.'
            continue
        json_body = [
            {
                "measurement": "yad",
                "tags": {
                    "account": account,
                    "login": wallet["login"]
                },
                "time": datetime.utcnow(),
                "fields": {
                    "available": available,
                    "total": total
                }
            }
        ]
        check_balance.influxdb.write_points(json_body)


db = mongodb_conn.mongodb.balancemon
check_balance.wallets = db.yad
check_balance.influxdb = influxdb_conn.influxdb
try:
    periodic_queue(60, check_balance)
except KeyboardInterrupt:
    print('^C received, exiting')
