# -*- coding: utf-8 -*-
import json
from queue import periodic_queue
from http_request import Http_request
from datetime import datetime
import mongodb_conn
import influxdb_conn


def check_balance():
    for wallet in check_balance.wallets.find():
        url = "https://rocketbank.ru/api/v5/login?apn_token={:s}".format(wallet["apn_token"])
        data = Http_request.request(url, None, ['x-sig:{:s}'.format(wallet["x-sig"]),
                                                'x-time:{:s}'.format(wallet["x-time"]),
                                                'x-device-id:{:s}'.format(wallet["x-device-id"]),
                                                'Authorization:{:s}'.format(wallet["Authorization"])])
        try:
            account_info_resp = json.loads(data)
            accounts = account_info_resp["user"]["accounts"]
            accounts.extend(account_info_resp["user"]["safe_accounts"])
            for account in accounts:
                json_body = [
                    {
                        "measurement": "rocket",
                        "tags": {
                            "owner": wallet["account"],
                            "account": account["account_details"]["account"],
                            "currency": account["currency"],
                            "title": account["title"]
                        },
                        "time": datetime.utcnow(),
                        "fields": {
                            "balance": account["balance"],
                        }
                    }
                ]
                check_balance.influxdb.write_points(json_body)

        except NameError:
            print 'Responce parsing error.'
            continue


db = mongodb_conn.mongodb.balancemon
check_balance.wallets = db.rocket
check_balance.influxdb = influxdb_conn.influxdb
try:
    periodic_queue(120, check_balance)
except KeyboardInterrupt:
    print('^C received, exiting')
