# -*- coding: utf-8 -*-
import json
from urllib import urlencode
from queue import periodic_queue
from http_request import Http_request
from datetime import datetime
import mongodb_conn
import influxdb_conn


def check_balance():
    for wallet in check_balance.wallets.find():
        url = "https://api.tinkoff.ru/v1/grouped_requests?"                 \
              "deviceId={:s}"                                               \
              "&sessionid={:s}"                                             \
              "&appVersion=4.3.2"                                           \
              "&origin=mobile%2Cib5%2Cloyalty%2Cplatform"                   \
              "&platform=ios"                                               \
              "&cpswc=true"                                                 \
              "&ccc=true"                                                   \
              "&connectionType=WiFi"                                        \
              .format(wallet["device_id"], wallet["session_id"])
        post = [{"key": "accounts", "operation": "accounts_flat"}]
        data = Http_request.request(url, urlencode({"requestsData": json.dumps(post)}),
                                    ["Content-Type:application/x-www-form-urlencoded; charset=utf-8;"])
        try:
            account_info_resp = json.loads(data)
            for account in account_info_resp["payload"]["accounts"]["payload"]:
                if "NORM" != account["status"]:
                    continue
                if "Credit" == account["accountType"]:
                    continue
                if "externalAccountNumber" not in account:
                    account["externalAccountNumber"] = account["id"]
                json_body = [
                    {
                        "measurement": "tinkoff",
                        "tags": {
                            "owner": wallet["account"],
                            "account": account["externalAccountNumber"],
                            "currency": account["accountBalance"]["currency"]["name"],
                            "title": account["name"]
                        },
                        "time": datetime.utcnow(),
                        "fields": {
                            "balance": account["accountBalance"]["value"],
                        }
                    }
                ]
                check_balance.influxdb.write_points(json_body)

        except NameError:
            print 'Responce parsing error.'
            continue


db = mongodb_conn.mongodb.balancemon
check_balance.wallets = db.tinkoff
check_balance.influxdb = influxdb_conn.influxdb
try:
    periodic_queue(60, check_balance)
except KeyboardInterrupt:
    print('^C received, exiting')
