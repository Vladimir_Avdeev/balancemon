import pycurl
try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO

class Http_request(object):
    @staticmethod
    def request(url, post=None, headers=[], ua="Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"):
        buffer = BytesIO()
        c = pycurl.Curl()
        c.setopt(c.URL, url)
        c.setopt(c.FOLLOWLOCATION, True)
        c.setopt(c.WRITEFUNCTION, buffer.write)
        c.setopt(c.ENCODING, 'gzip')
        c.setopt(c.USERAGENT, ua)
        if post is not None:
            c.setopt(c.POST, True)
            c.setopt(c.POSTFIELDS, post)
        if headers is not []:
            c.setopt(c.HTTPHEADER, headers)
        c.perform()
        c.close()
        return buffer.getvalue()

    @staticmethod
    def clear_cookie():
        from pathlib import Path
        cookie_file = Path('/tmp/cookie')
        if cookie_file.is_file():
            cookie_file.unlink()