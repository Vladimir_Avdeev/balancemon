#!/bin/bash
if pgrep -x -f "python /scripts/balancemon/yad.py" > /dev/null
then
  echo "Running"
else
  echo "Stopped"
  python /scripts/balancemon/yad.py > /dev/null &
fi
if pgrep -x -f "python /scripts/balancemon/rocket.py" > /dev/null
then
  echo "Running"
else
  echo "Stopped"
  python /scripts/balancemon/rocket.py > /dev/null &
fi
if pgrep -x -f "python /scripts/balancemon/homecredit.py" > /dev/null
then
  echo "Running"
else
  echo "Stopped"
  python /scripts/balancemon/homecredit.py > /dev/null &
fi
if pgrep -x -f "python /scripts/balancemon/tinkoff.py" > /dev/null
then
  echo "Running"
else
  echo "Stopped"
  python /scripts/balancemon/tinkoff.py > /dev/null &
fi
