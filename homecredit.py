# -*- coding: utf-8 -*-
import json
from queue import periodic_queue
from http_request import Http_request
from datetime import datetime
import mongodb_conn
import influxdb_conn



def check_balance():
    for wallet in check_balance.wallets.find():
        url = "https://ib.homecredit.ru/mobile/remoting/ProductService"
        data = Http_request.request(url,
                                    '{"arguments": [], \
                                        "parameterTypes": [], \
                                        "methodName": "getAllProducts", \
                                        "attributes": null, \
                                        "javaClass": "org.springframework.remoting.support.RemoteInvocation"}:',
                                    ['Content-Type:application/x-www-form-urlencoded',
                                     'Cookie:{:s}'.format(wallet["Cookie"])],
                                    "Home%20Credit/300 CFNetwork/901.1 Darwin/17.6.0")
        try:
            account_info_resp = json.loads(data)
            accounts = account_info_resp["debitCards"]["list"]
            accounts.extend(account_info_resp["creditCards"]["list"])
            accounts.extend(account_info_resp["credits"]["list"])
            accounts.extend(account_info_resp["deposits"]["list"])
            accounts.extend(account_info_resp["merchantCards"]["list"])
            for account in accounts:
                json_body = [
                    {
                        "measurement": "homecredit",
                        "tags": {
                            "owner": wallet["account"],
                            "account": account["accountNumber"],
                            "currency": account["currency"],
                            "title": account["productName"]
                        },
                        "time": datetime.utcnow(),
                        "fields": {
                            "balance": account["availableBalance"],
                        }
                    }
                ]
                check_balance.influxdb.write_points(json_body)

        except NameError:
            print 'Responce parsing error.'
            continue


db = mongodb_conn.mongodb.balancemon
check_balance.wallets = db.homecredit
check_balance.influxdb = influxdb_conn.influxdb
try:
    periodic_queue(120, check_balance)
except KeyboardInterrupt:
    print('^C received, exiting')
