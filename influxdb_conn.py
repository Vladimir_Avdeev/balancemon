import config as c
from influxdb import InfluxDBClient

influxdb = InfluxDBClient(c.INFLUXDB_HOST, c.INFLUXDB_PORT, c.INFLUXDB_USER, c.INFLUXDB_PASS, c.INFLUXDB_BASE)
